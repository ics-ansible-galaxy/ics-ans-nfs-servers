# ics-ans-nfs-servers

Ansible playbook to install nfs-servers.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
